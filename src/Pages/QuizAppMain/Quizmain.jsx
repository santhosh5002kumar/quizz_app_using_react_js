import React, { useContext, useState } from 'react'
import { Typead } from '../../Components/Context/Context';
import { quizQuestions } from '../../Components/Quesitons/Qns';
import './Quizmain.css';
import NewUSer from '../../Components/NewUser/NewUSer';


export default function Quizmain(props) {
  // =======================================================================Through UseContext
  const {score,changeScore,start,changeStart}=useContext(Typead);
  // ========================================================================States
    const [questionidx, changeQuestion] = useState(0)
    const [showScore,changeShow]=useState(false)
    let curentQuestion=quizQuestions[questionidx]
  // ===================================================Option Validation
    const crctanswer=(index)=>{
      if(curentQuestion.answer==index){
        changeScore(score+1); 
      }
      const nextQ=questionidx+1
        if(nextQ < quizQuestions.length){
          changeQuestion(questionidx+1);
        }
        else{
          changeShow(true)
        }
    }
    // =======================================================Reset button
    const reset=()=>{
      changeStart(false)
      changeScore(0);
      changeQuestion(0);
      changeShow(false);
    }
    const user=localStorage.getItem("active_User")
  
    return (

        <div className='quiz_container'>
          {start?<div className='quiz_container'>
          { showScore?
          <>
          <h1>Your Score Is :{score}</h1>
          <button onClick={reset}>Restart</button>
          </>
          :
          <div>
            <h3>{user} is Playing !</h3>
          <h4>Question Number: {curentQuestion.qno}</h4>
          <h2>{curentQuestion.question}</h2>
          <div className='question_options'>
        {curentQuestion.ansOptions.map((opt,index)=>{ return <span onClick={()=>crctanswer(index)}>{opt}</span>
        })}
          </div>
          <div className='butn_grp'>
            <button className='next'>Next</button>
            <button className='prev'>Previous</button>
          </div>
          </div>
          }
          </div>
          :<NewUSer />}
        </div>
    );
}
