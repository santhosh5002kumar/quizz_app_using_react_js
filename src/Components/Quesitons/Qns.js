export const quizQuestions=[
    {
        qno:1,
        question:"React Is a?",
        ansOptions:["Framework Of Javascript Library","Complete Independent Library","Scripted Library","HTML Library"],
        answer:0
    },
    {
        qno:2,
        question:"What is The Full Form of CSS",
        ansOptions:["Cascading Style Spreadsheet","Cascading Style Sheet","Connected Styling Sheets","None of The Above"],
        answer:1
    },
    {
        qno:3,
        question:"99/5+69-0.75/34*300",
        ansOptions:["578.34","489.13","90.51","100.56"],
        answer:0
    },
    {
        qno:4,
        question:"Exact Value of PI",
        ansOptions:["3.411592359","3.1545359","3.14826563","3.14159265359"],
        answer:3
    },
    {
        qno:5,
        question:"What is GOOGLE",
        ansOptions:["Gathered Organization of Oriented Group Language of Earth","Global Organization of Oriented Group Language of Earth","Global Organization of Oriented Group Literature of Entire","None of The Above"],
        answer:1
    },
    {
        qno:6,
        question:"What is Full Form Of DELL",
        ansOptions:["Direct Electronic Link Lockers","Digital Essential Link Library","Digital Electronic Link Library","Delicate Electronic Lifetime Library"],
        answer:3
    },
]