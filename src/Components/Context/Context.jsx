import { createContext, useRef, useState } from "react";
import Quizmain from "../../Pages/QuizAppMain/Quizmain";
import NewUSer from "../NewUser/NewUSer";

export const Typead=createContext("")
export const Storage=()=>{
    const [score,changeScore]=useState(0)
    const [start,changeStart]=useState(false)
    const userName=useRef()
    // =====================================================================
    const gameStart=()=>{

        let NewUser=JSON.parse(localStorage.getItem("users")||("[]"));
        const Users={
            name:userName.current.value,
            FinalScore:score,
        }
        const ActiveUser=userName.current.value
        NewUser.push(Users)
        localStorage.setItem("users",JSON.stringify(NewUser))
        localStorage.setItem("active_User",ActiveUser)

        changeStart(true)
    }
   return(
    <>
    <Typead.Provider value={{score,changeScore,userName,changeStart,start,gameStart}}>
    <Quizmain />
    </Typead.Provider>
    </>
   )
}
