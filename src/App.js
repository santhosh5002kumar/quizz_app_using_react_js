
import Quizmain from './Pages/QuizAppMain/Quizmain';
import { Storage } from './Components/Context/Context';


function App() {
  return (
    <div>
    <Storage />
    </div>
  );
}

export default App;
