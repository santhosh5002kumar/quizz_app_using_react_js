import React, { useContext, useState } from 'react'
import { Typead } from '../Context/Context'

export default function NewUSer() {
    const {userName,changeStart,score,gameStart}=useContext(Typead)

  return (
    <div>
        <h1>Quiz Round</h1>
        <h1>Enter Your Name</h1>
        <input type={"text"} ref={userName}/>
        <button onClick={gameStart}>Click to Start</button>
    </div>
  )
}

